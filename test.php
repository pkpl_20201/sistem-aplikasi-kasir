<?php include "connect/koneksi.php" 
	  include " transaksi.php"
	  ?>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" href="asset/font-awesome/css/font-awesome.min.css">

<body>
	<div class="MockHTTPService">
		def_init(self, expected_output):
		self.output = expected_output

		 @coroutine
    def fetch(self, base_endpoint, **kwargs):
        raise Return(self.output)
		

	</div>
	<div class="TestLib1">
		@gen_test
    def test_service_unknown(self):

        payload = dict(
            key1 = "testing",
            key2 = "test@test.com",
            key3 = "testing"
        )

        expected_output = MockResponse()
        expected_output.error = 'Something error'

        http = MockHTTPService(expected_output)
        lib1 = Lib1(http)

        response = yield lib1.custom_method(payload)
        self.assertTrue(response['is_error'])
        self.assertEqual('Something error', response['error_message'])


	</div>

</body>>
