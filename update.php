<?php include "connect/koneksi.php" ?>
<!DOCTYPE html>
<html>
<head>
        <title>Update Barang</title>
        <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.css">
    </head>

    <body class="container">
        <h1 align="center">Update Barang</h1>
        <form action="update_p.php" method="post" enctype="multipart/form-data">
            <table class="table">
                <tr>
                    <td>Kode Buku</td>
                    <td>Nama Buku</td>
                    <td>Penulis</td>
                    <td>Penerbit</td>
                    <td>Tersedia</td>
                    <td>Harga</td>
                </tr>
                <tr>
                    <?php
                        $kodebuku = $_POST['kd'];
                        $lihat_1 = mysql_query("SELECT * FROM daftarbuku WHERE daftarbuku.kodebuku = '$kodebuku'");
                        $arr_lihat1 = mysql_fetch_array($lihat_1);
                    ?>
                    <td>
                        <input type="text" class="form-control" value="<?php echo $arr_lihat1['kodebuku']; ?>" disabled="disabled" />
						<input type="hidden" name="kodebuku" value="<?php echo $arr_lihat1['kodebuku']; ?>" />
                    </td>
                    <td>
                        <input type="text" class="form-control" name="namabuku" value="<?php echo $arr_lihat1['namabuku']; ?>" />
                    </td>
                    <td>
                        <input type="text" class="form-control" name="penulis" value="<?php echo $arr_lihat1['penulis']; ?>"  />
                    </td>
                    <td>
                        <input type="text" class="form-control" name="penerbit" value="<?php echo $arr_lihat1['penerbit']; ?>" />
                    </td>
                    <td>
                        <input type="text" class="form-control" name="tersedia" value="<?php echo $arr_lihat1['tersedia']; ?>" />
                    </td>
                    <td>
                        <input type="text" class="form-control" name="harga" value="<?php echo $arr_lihat1['harga']; ?>" />
                    </td>
                    <td>
                        <input type="submit" class="btn btn-primary" value="Update" />
                    </td>
                </tr>
            </table>
        </form>

        <?php include "footer.html"; ?>

    </body>
</html>